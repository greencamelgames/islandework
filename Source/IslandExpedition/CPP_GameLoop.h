// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_GameLoop.generated.h"

UCLASS()
class ISLANDEXPEDITION_API ACPP_GameLoop : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_GameLoop();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
		int currentTurn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
		int currentGold;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
		int goldFlow;

	UFUNCTION(BlueprintCallable)
		void NextTurn();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
