// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_BasicShip.h"


// Sets default values
ACPP_BasicShip::ACPP_BasicShip()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;
	//Ship move varibles
	firstClick = false;
	shipMaxMovesInt = 5;
	shipMovesLeft = shipMaxMovesInt;
	moveCost = 0;
	moveXDifInt = 0;
	moveYDifInt = 0;
	//ship stats
	maxCrewRequired = 12;
	captainNeeded = false;
	foodAvailableOnBoard = 0;

	storageSpaceAvailable = 50;
	goldPerTurn = 50;
	foodRequiredOnBoard = 50;
	requirementsMet = false;

	movementBonus = 0;
}

int ACPP_BasicShip::getUnitMaxMovesBoat(void)
{
	if (unitMaxMovesInt == 3)
	{
		return true;
	}
	return unitMaxMovesInt;
}

// Called when the game starts or when spawned
void ACPP_BasicShip::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACPP_BasicShip::ResetTurn(int initShipMaxMoves)		//resets the moves
{
	shipMovesLeft = initShipMaxMoves;
}

void ACPP_BasicShip::ClickDistance(int oldXPosInt, int oldYPosInt, int newXPos, int newYPos)		//works out how much it will take to move to that clcik location
{
	moveXDifInt = newXPos - oldXPosInt;
	moveYDifInt = newYPos - oldYPosInt;
	if (moveXDifInt < 0)
	{
		moveXDifInt = moveXDifInt * -1;
	}
	if (moveYDifInt < 0)
	{
		moveYDifInt = moveYDifInt * -1;
	}
	moveCost = moveXDifInt + moveYDifInt;

}

bool ACPP_BasicShip::WithinRange(int initMoveCost, int initMovesLeft)		//checks the tile is within range to move
{
	if (initMoveCost > initMovesLeft)
	{
		return false;
	}
	else
	{
		shipMovesLeft = initMovesLeft - initMoveCost;
		return true;
	}	
}

// Called every frame
void ACPP_BasicShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

