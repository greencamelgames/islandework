// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Tile.generated.h"

UCLASS()
class ISLANDEXPEDITION_API ACPP_Tile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_Tile();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile Values")
		bool tileSectedBool;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile Values")
		int islandIDInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile Values")
		int tileOwnerInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile Values")
		int tileTerrianTypeInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile Values")
		int tileResourceTypeInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile Values")
		int tileIDInt;



	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	
	
	
};
