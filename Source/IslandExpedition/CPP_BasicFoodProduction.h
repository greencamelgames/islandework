// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_BasicFoodProduction.generated.h"

UCLASS()
class ISLANDEXPEDITION_API ACPP_BasicFoodProduction : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_BasicFoodProduction();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Per Turn Stats")
		int foodGenPerTurnInt;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
