// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_City.generated.h"

UCLASS()
class ISLANDEXPEDITION_API ACPP_City : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_City();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Stats")
		int cityIDInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Stats")
		int cityOwnerInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Stats")
		int islandIDInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Population")
		int cityPopulationInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Population")
		int turnsUntilGrowthMaxInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Population")
		int turnsUntilGrowthLeftInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Population")
		int cityPopulationIncreaseInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Food")
		int cityFoodPTInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Food")
		int cityFoodIncreasePTInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Iron")
		int cityIronPTInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Iron")
		int cityIronIncreasePTInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Gold")
		int cityGoldPTInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Gold")
		int cityGoldIncreasePTInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Recources")
		int woodInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Recources")
		int metalInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Recources")
		int goldInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Recources")
		int rareGoodsInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "City Recources")
		int foodInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Worker Build Stats")
		int actionTimeMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Worker Build Stats")
		int actionTimeLeft;
	UFUNCTION(BlueprintCallable)
		int GetWood();

	UFUNCTION(BlueprintCallable)
		void SetWood(int initAmountToAdd);
	UFUNCTION(BlueprintCallable)
		void ResetActionTime(int timeLeft);

	UFUNCTION(BlueprintCallable)
		void UpdatePopulation(int populationChange);
	UFUNCTION(BlueprintCallable)
		void UpdateFoodPT(int foodChange);
	UFUNCTION(BlueprintCallable)
		void GenFoodPT();

	UFUNCTION(BlueprintCallable)
		void GenGoldPT();
	UFUNCTION(BlueprintCallable)
		void GenIronPT();

	UFUNCTION(BlueprintCallable)
		void ResetCityGrowthTime(int timeLeft);
	UFUNCTION(BlueprintCallable)
		void MakeCityGrow(int populationChange);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
