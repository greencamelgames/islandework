// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Map.generated.h"


UCLASS()
class ISLANDEXPEDITION_API ACPP_Map : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_Map();
	


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGrid")
		int mapSizeXInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGrid")
		int mapSizeYInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGrid")
		int gridLocationXInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGrid")
		int gridLocationYInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGrid")
		int tileIDInt;

	UFUNCTION(BlueprintCallable)
		int GetTileType(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		void FillRandomlyGenMap();
	UFUNCTION(BlueprintCallable)
		int GetTileIslandID();
	UFUNCTION(BlueprintCallable)
		int GetIslandIDFromLocation(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		int GetTileOwner(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		int GetTileTerrian();
	UFUNCTION(BlueprintCallable)
		void SetTileTerrian(int initXPosInt, int initYPosInt, int initNewTileTerrian);
	UFUNCTION(BlueprintCallable)
		int GetTileModel(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		int GetTileRescource();
	UFUNCTION(BlueprintCallable)
		void SetTileID(int initTileIDInt);
	UFUNCTION(BlueprintCallable)
		int GetTileID(int initXPosInt, int initYPosInt);

	//ship movement
	UFUNCTION(BlueprintCallable)
		int GetShipLocation(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		void CreateShip(int initXPosInt, int initYPosInt, int initShipID, int initShipTypeInt);
	UFUNCTION(BlueprintCallable)
		void UpdateShipLocation(int initNewXPos, int initNewYPos, int initUnitID, int initUnitTypeInt);
	UFUNCTION(BlueprintCallable)
		int GetShipIDInTile(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		int GetShipType(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		bool ShipInTile(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		bool ShipSameTile(int initXPos, int initYPos);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGrid")
		int oldShipXPosInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGrid")
		int oldShipYPosInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int shipCount;

	//unit movement
	UFUNCTION(BlueprintCallable)
		int GetUnitLocation(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		void CreateUnit(int initXPosInt, int initYPosInt, int initUnitID,int initUnitTypeInt, int setMovement);
	UFUNCTION(BlueprintCallable)
		void UpdateUnitLocation(int initNewXPos, int initNewYPos, int initUnitID, int initUnitTypeInt);
	UFUNCTION(BlueprintCallable)
		int GetUnitIDInTile(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		int GetUnitType(int initXPosInt, int initYPosInt);
	UFUNCTION(BlueprintCallable)
		bool UnitInTile(int initXPos, int initYPos);
	UFUNCTION(BlueprintCallable)
		bool UnitSameTile(int initXPos, int initYPos);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int unitCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int oldUnitXPosInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int oldUnitYPosInt;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MapGrid")
		bool firstClick;

	//BuildingSpawn
	UFUNCTION(BlueprintCallable)
		bool checkCoast(int initXPos, int initYPos);
	UFUNCTION(BlueprintCallable)
		int getHarbourRotation(int initXPos, int initYPos);

	UFUNCTION(BlueprintCallable)
		int getFirstCityActualX();

	UFUNCTION(BlueprintCallable)
		int getFirstCityActualY();

	UFUNCTION(BlueprintCallable)
		int getCityCount();

	UFUNCTION(BlueprintCallable)
		int getNumberOfLandUnits();

	UFUNCTION(BlueprintCallable)
		int getNumberOfSeaUnits();

	UFUNCTION(BlueprintCallable)
		int getNumberOfLandUnitsWithMovementLeft();

	UFUNCTION(BlueprintCallable)
		int getNumberOfSeaUnitsWithMovementLeft();

	UFUNCTION(BlueprintCallable)
		int getUnitExactXLocation(int initXPosInt);

	UFUNCTION(BlueprintCallable)
		int getUnitExactYLocation(int initYPosInt);

	UFUNCTION(BlueprintCallable)
		void setLandUnitsMovement(int unitType, int unitID);

	UFUNCTION(BlueprintCallable)
		void setSeaUnitsMovement(int seaUnitType, int seaUnitID);

	UFUNCTION(BlueprintCallable)
		bool checkBoatInTile(int initXPos, int initYPos);

	/*UFUNCTION(BlueprintCallable)
		bool checkClickNextToLand(int initXPos, int initYPos);*/

	UFUNCTION(BlueprintCallable)
		int CheckClickNextToLandTile(int initXPos, int initYPos);

	UFUNCTION(BlueprintCallable)
		bool checkBoatNextToLand(int initXPos, int initYPos);

	UFUNCTION(BlueprintCallable)
		void setNewModel(int initXPos, int initYPos, int initModelType);
	UFUNCTION(BlueprintCallable)
		bool checkModelInTile(int initXPos, int initYPos);

	UFUNCTION(BlueprintCallable)
		void setIslandOwner(int initIslandIDInt, int initIslandOwner);

	UFUNCTION(BlueprintCallable)
		int getDisembarkXPos();

	UFUNCTION(BlueprintCallable)
		int getDisembarkYPos();

	UFUNCTION(BlueprintCallable)
		void clearUnitFromArray(int initXPosInt, int initYPosInt);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EmbarkShipLocation")
		int embarkXPosInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EmbarkShipLocation")
		int embarkYPosInt;

	UFUNCTION(BlueprintCallable)
		int getCityTileIDOnIsland(int tileOwnerID, int initIslandID);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DisembarkShipLocation")
		int disembarkXPosInt;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DisembarkShipLocation")
		int disembarkYPosInt;


//public:	
//	// Called every frame
//	virtual void Tick(float DeltaTime) override;
private:
	int firstArrayInt[31][31][2];
	int mapArrayInt[31][31][7];
	int modelArrayInt[31][31][2];
	int shipMovementArrayInt[31][31][4];
	int unitMovementArrayInt[31][31][4];
	int islandSizeXInt;
	int islandSizeYInt;
	int islandCountInt;
	int islandIDInt;
	int playerCountInt;
	int playerMaxAmountInt;

	int failedCreateIslandInt;
	int failedResize;

	int oldIslandSizeXInt;
	int oldIslandSizeYInt;

	int mapSizeHighXInt;
	int mapSizeLowXInt;
	int mapSizeHighYInt;
	int mapSizeLowYInt;
	int x;
	int y;

	int randXPosInt;
	int randYPosInt;
	int randStartXPosInt;
	int randStartYPosInt;

	int checkStartXPosInt;
	int checkStartYPosInt;

	int spawnRandXPosInt;
	int spawnRandYPosInt;
	int spawnTilesCountInt;
	int spawnIslandMaxTilesInt;
	int spawnIslandMinTilesInt;

	int spawnHighXPosInt;
	int spawnLowXPosInt;
	int spawnHighYPosInt;
	int spawnLowYPosInt;

	int bigIslandCountInt;
	int bigIslandMaxint;
	int bigIslandMaxSizeXInt;
	int bigIslandMinSizeXInt;
	int bigIslandMaxSizeYInt;
	int bigIslandMinSizeYInt;

	int mediumIslandCountInt;
	int mediumIslandMaxint;
	int mediumIslandMaxSizeXInt;
	int mediumIslandMinSizeXInt;
	int mediumIslandMaxSizeYInt;
	int mediumIslandMinSizeYInt;

	int rainForestChanceInt;

	int metalMaxChanceInt;
	int metalSpawnMaxInt;
	int goldMaxSpawnChanceInt;
	int goldSpawnMaxInt;

	int islandAmountInt;

	int firstCityX;
	int firstCityY;

	int cityCount;

	bool CreateIslandSpace(int initIslandMaxSizeXInt, int initIslandMinSizeXInt, int initIslandMaxSizeYInt, int initIslandMinSizeYInt, int initIslandCountInt, int initislandMaxInt);
	void CreateIslandSize(int initIslandMaxSizeXInt, int initIslandMinSizeXInt, int initIslandMaxSizeYInt, int initIslandMinSizeYInt);
	bool CheckIslandArea(int initXPosInt, int initYPosInt, int initIsalndSizeXInt, int initIslandSizeYInt);
	void SpawnIsland(int initStartXPosInt, int initStartYPosInt, int initIslandSizeXInt, int initIslandSizeYInt, int initTileAmountInt);
	void SpawnCity();
	
	void AdujstMapSize();	
	int SpawnForest(int initRainForestChance);
	bool CheckIslandForResources(int initIslandID,int initRescourceID);
	void SpawnResources(int initSpawnAmountInt, int initSpawnMaxAmountInt, int initIslandSpawnCountInt, int initIslandMaxSpawnableInt, int initSpawnIslandIDInt
		, int initIslandFailedCountInt, bool initSpawnedOnIslandBool, int initResourceIDInt, int initSpawnChanceInt, int initSpawnMaxChanceInt);



};
	
	

