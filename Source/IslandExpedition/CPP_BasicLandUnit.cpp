// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_BasicLandUnit.h"


// Sets default values
ACPP_BasicLandUnit::ACPP_BasicLandUnit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	firstClick = false;
	unitMaxMovesInt = 3;
	unitMovesLeftInt = unitMaxMovesInt;
	moveCost = 0;
	moveXDifInt = 0;
	moveYDifInt = 0;
	unitID = 0;
	movementAllowed = 1;


}

// Called when the game starts or when spawned
void ACPP_BasicLandUnit::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACPP_BasicLandUnit::ResetTurn(int initUnitMaxMoves)
{
	unitMovesLeftInt = initUnitMaxMoves;
}

void ACPP_BasicLandUnit::checkMovement()
{
	if (unitMovesLeftInt > 0) {
		movementAllowed = 1;
	}
	else if (unitMovesLeftInt == 0) {
		movementAllowed = 0;
	};
}

void ACPP_BasicLandUnit::ClickDistance(int oldXPosInt, int oldYPosInt, int newXPos, int newYPos)
{
	moveXDifInt = newXPos - oldXPosInt;
	moveYDifInt = newYPos - oldYPosInt;
	if (moveXDifInt < 0)
	{
		moveXDifInt = moveXDifInt * -1;
	}
	if (moveYDifInt < 0)
	{
		moveYDifInt = moveYDifInt * -1;
	}
	moveCost = moveXDifInt + moveYDifInt;
}

bool ACPP_BasicLandUnit::WithinRange(int initMoveCost, int initMovesLeft)
{
	if (initMoveCost > initMovesLeft)
	{
		return false;
	}
	else
	{
		unitMovesLeftInt = initMovesLeft - initMoveCost;
		return true;
	}
}

int ACPP_BasicLandUnit::getMovement()
{
	return movementAllowed;
}

void ACPP_BasicLandUnit::setMovement(int allowed)
{
	movementAllowed = allowed;
}


// Called every frame
void ACPP_BasicLandUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

