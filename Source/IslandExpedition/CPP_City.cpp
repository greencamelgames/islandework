// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_City.h"


// Sets default values
ACPP_City::ACPP_City()
{

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	woodInt = 0;
	metalInt = 0;
	goldInt = 0;
	rareGoodsInt = 0;
	foodInt = 0;
	actionTimeMax = 4;
	actionTimeLeft = actionTimeMax;

	cityPopulationInt = 10;
	cityFoodPTInt = 20;

	cityPopulationIncreaseInt = 2;

	turnsUntilGrowthMaxInt = 5;
	turnsUntilGrowthLeftInt = turnsUntilGrowthMaxInt;

	cityFoodIncreasePTInt = 0;

	cityGoldPTInt = 0;
	cityGoldIncreasePTInt = 0;

	cityIronPTInt = 0;
	cityIronIncreasePTInt = 0;

}

// Called when the game starts or when spawned
void ACPP_City::BeginPlay()
{
	Super::BeginPlay();
	
}

int ACPP_City::GetWood()
{
	return woodInt;
}


void ACPP_City::SetWood(int initAmountToAdd)
{
	woodInt = woodInt + initAmountToAdd;
}

void ACPP_City::UpdatePopulation(int populationChange)
{
	cityPopulationInt = cityPopulationInt - populationChange;
}

void ACPP_City::MakeCityGrow(int populationChange)
{
	cityPopulationInt = cityPopulationInt + populationChange;
}

void ACPP_City::UpdateFoodPT(int foodChange)
{
	cityFoodPTInt = cityFoodPTInt - foodChange;
}

void ACPP_City::GenFoodPT()
{
	cityFoodPTInt = cityFoodPTInt + cityFoodIncreasePTInt;
}

void ACPP_City::GenGoldPT()
{
	cityGoldPTInt = cityGoldPTInt + cityGoldIncreasePTInt;
	goldInt = cityGoldPTInt;
}

void ACPP_City::GenIronPT()
{
	cityIronPTInt = cityIronPTInt + cityIronIncreasePTInt;
	metalInt = cityIronPTInt;
}

void ACPP_City::ResetActionTime(int timeLeft)
{
	actionTimeLeft = timeLeft;
}

void ACPP_City::ResetCityGrowthTime(int timeLeft)
{
	turnsUntilGrowthLeftInt = timeLeft;
}

// Called every frame
void ACPP_City::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

