// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_MineMetal.generated.h"

UCLASS()
class ISLANDEXPEDITION_API ACPP_MineMetal : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_MineMetal();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Per Turn Stats")
		int metalGenPerTurnInt;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
