// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_Map.h"
#include <time.h>

// Sets default values
ACPP_Map::ACPP_Map()
{
	mapSizeXInt = 31;
	mapSizeYInt = 31;
	gridLocationXInt = 1;
	gridLocationYInt = 1;
	tileIDInt = 0;

	failedCreateIslandInt = 0;
	failedResize = 0;

	firstArrayInt[31][31][1] = 0;
	islandSizeXInt = 0;
	islandSizeYInt = 0;
	islandCountInt = 0;

	oldIslandSizeXInt = 0;
	oldIslandSizeYInt = 0;

	mapSizeHighXInt = 0;
	mapSizeLowXInt = 0;
	mapSizeHighYInt = 0;
	mapSizeLowYInt = 0;
	x = 0;
	y = 0;
	randXPosInt = 0;
	randYPosInt = 0;

	checkStartXPosInt = 0;
	checkStartYPosInt = 0;

	spawnRandXPosInt = 0;
	spawnRandYPosInt = 0;
	spawnTilesCountInt = 0;


	spawnHighXPosInt = 0;
	spawnLowXPosInt = 0;
	spawnHighYPosInt = 0;
	spawnLowYPosInt = 0;

	bigIslandCountInt = 0;
	bigIslandMaxint = 3;
	bigIslandMaxSizeXInt = 10;
	bigIslandMinSizeXInt = 6;
	bigIslandMaxSizeYInt = 10;
	bigIslandMinSizeYInt = 6;

	mediumIslandCountInt = 0;
	mediumIslandMaxint = 5;
	mediumIslandMaxSizeXInt = 8;
	mediumIslandMinSizeXInt = 5;
	mediumIslandMaxSizeYInt = 8;
	mediumIslandMinSizeYInt = 5;
	oldShipXPosInt = 0;
	oldShipYPosInt = 0;
	oldUnitXPosInt = 0;
	oldUnitYPosInt = 0;

	islandIDInt = 0;
	playerCountInt = 0;
	playerMaxAmountInt = 1;

	rainForestChanceInt = 0;
	metalSpawnMaxInt = 6;
	metalMaxChanceInt = 2;
	goldMaxSpawnChanceInt = 2;
	goldSpawnMaxInt = 3;


	islandAmountInt = 0;

	//EmbarkLocation
	embarkXPosInt = 0;
	embarkYPosInt = 0;

	//disembarkLocation
	disembarkXPosInt = 0;
	disembarkYPosInt = 0;

	firstCityX = 0;
	firstCityY = 0;

	firstClick = true;
	unitCount = 0;

	cityCount = 0;

	while (x < (mapSizeXInt - 1))	////fills array with zeros
	{
		x++;
		y = 0;
		while (y < (mapSizeYInt - 1))
		{
			y++;
			firstArrayInt[x][y][1] = 0;
			mapArrayInt[x][y][1] = 0;
			modelArrayInt[x][y][1] = 0;
			shipMovementArrayInt[x][y][1] = 0;
			unitMovementArrayInt[x][y][1] = 0;
			shipMovementArrayInt[x][y][3] = 0;
			unitMovementArrayInt[x][y][3] = 0;
		}
	}
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

}

void ACPP_Map::FillRandomlyGenMap()
{
	
	while (bigIslandCountInt < bigIslandMaxint)	///spawns big islands
	{
		
		if (failedResize < 20)		//if its failed to spawn a big island x amount of times skip spawing island
		{
			if (CreateIslandSpace(bigIslandMaxSizeXInt, bigIslandMinSizeXInt, bigIslandMaxSizeYInt, bigIslandMinSizeYInt, bigIslandCountInt, bigIslandMaxint) == true)		//creates the island space
			{
				bigIslandCountInt++;		//successfully spawn and island space
				failedResize = 0;
			}
			else
			{
				failedResize++;
			}

		}
		else
		{
			break;
		}

	}

	islandCountInt = 0;
	islandSizeXInt = 0;
	islandSizeYInt = 0;
	randXPosInt = 0;
	randYPosInt = 0;
	

	while (mediumIslandCountInt < mediumIslandMaxint)		//same as above but for medium islands
	{
		if (failedResize < 20)
		{
			if (CreateIslandSpace(mediumIslandMaxSizeXInt, mediumIslandMinSizeXInt, mediumIslandMaxSizeYInt, mediumIslandMinSizeYInt, mediumIslandCountInt, mediumIslandMaxint) == true)
			{
				mediumIslandCountInt++;
				failedResize = 0;
			}
			else
			{
				failedResize++;
			}
			
		}
		else
		{
			break;
		}
	}	
	//Spawn resources onto map
	islandAmountInt = islandIDInt + 1;
	SpawnResources(0, metalSpawnMaxInt, 0, (islandAmountInt / 2), 0, 0, false, 1, 0, metalMaxChanceInt);	//spawns metal
	SpawnResources(0, goldSpawnMaxInt, 0, (islandAmountInt / 3), 0, 0, false, 2, 0, goldMaxSpawnChanceInt);	//spawns gold
}

///SHIP MOVEMENT
int ACPP_Map::GetShipLocation(int initXPosInt, int initYPosInt)
{
	return shipMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][1];		// returns the ship type
}

void ACPP_Map::CreateShip(int initXPosInt, int initYPosInt, int initShipID, int initShipTypeInt)		//creates a ship
{
	shipMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][1] = initShipTypeInt;		//sets the ship type
	shipMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][2] = initShipID;				//sets the shipID
}

void ACPP_Map::UpdateShipLocation(int initNewXPos, int initNewYPos, int initShipID, int initShipTypeInt)		//updates the ships location.
{
	shipMovementArrayInt[(initNewXPos - 2)][(initNewYPos - 2)][1] = initShipTypeInt;		//sets the new posistion in the array to have the ship details
	shipMovementArrayInt[(initNewXPos - 2)][(initNewYPos - 2)][2] = initShipID;
	shipMovementArrayInt[(oldShipXPosInt - 2)][(oldShipYPosInt - 2)][1] = 0;		//clears the old posistion
	shipMovementArrayInt[(oldShipXPosInt - 2)][(oldShipYPosInt - 2)][2] = 0;
}

int ACPP_Map::GetShipIDInTile(int initXPosInt, int initYPosInt)
{
	return shipMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][2];		//returns the ship ID
}

int ACPP_Map::GetShipType(int initXPosInt, int initYPosInt)
{
	return shipMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][1];		//returns the ship type
}

bool ACPP_Map::ShipInTile(int initXPosInt, int initYPosInt)
{
	if (shipMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][1] >= 1)		//checks if theirs a ship in that tile
	{
		return true;
	}
	return false;
}

bool ACPP_Map::ShipSameTile(int initXPos, int initYPos)			//checks if the new click is the same as the last click
{
	if (oldShipXPosInt == initXPos && oldShipYPosInt == initYPos)
	{
		return true;
	}
	return false;
}

////UNIT MOVEMENT
int ACPP_Map::GetUnitLocation(int initXPosInt, int initYPosInt)
{
	return unitMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][1];	//returns unit type
}

int ACPP_Map::getUnitExactXLocation(int initXPosInt)
{
	int ExactX = initXPosInt * 200;
	return ExactX;
}

int ACPP_Map::getUnitExactYLocation(int initYPosInt)
{
	int ExactY = initYPosInt * 200;
	return ExactY;
}

void ACPP_Map::CreateUnit(int initXPosInt, int initYPosInt, int initUnitID, int initUnitTypeInt, int setMovement)		//creates a Unit
{
	unitMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][1] = initUnitTypeInt;		//sets the unit type
	unitMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][2] = initUnitID;				//sets the unit ID
	unitMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][3] = setMovement;		//sets the unit type
}

void ACPP_Map::UpdateUnitLocation(int initNewXPos, int initNewYPos, int initUnitID, int initUnitTypeInt)		//update the unit posistion
{
	unitMovementArrayInt[(initNewXPos - 2)][(initNewYPos - 2)][1] = initUnitTypeInt;			//sets unit details for new position
	unitMovementArrayInt[(initNewXPos - 2)][(initNewYPos - 2)][2] = initUnitID;
	unitMovementArrayInt[(oldUnitXPosInt - 2)][(oldUnitYPosInt - 2)][1] = 0;			//clears unit details from old location
	unitMovementArrayInt[(oldUnitXPosInt - 2)][(oldUnitYPosInt - 2)][2] = 0;
}

int ACPP_Map::GetUnitIDInTile(int initXPosInt, int initYPosInt)
{
	return unitMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][2];		//returns unit ID
}

int ACPP_Map::GetUnitType(int initXPosInt, int initYPosInt)
{
	return unitMovementArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][1];		//retruns unit type
}

bool ACPP_Map::UnitInTile(int initXPos, int initYPos)
{
	if (unitMovementArrayInt[(initXPos - 2)][initYPos - 2][1] >= 1)			//checks if their is a unit in new tile
	{
		return true;
	}
	return false;
}

bool ACPP_Map::UnitSameTile(int initXPos, int initYPos)
{
	if (oldUnitXPosInt == initXPos && oldUnitYPosInt == initYPos)			//checks if the second click is the same tile
	{
		return true;
	}
	return false;
}

bool ACPP_Map::checkCoast(int initXPos, int initYPos)                    //checks if click is on a sea tile
{
	int originXInit = initXPos -2;
	int originYInit = initYPos -2;

	if (mapArrayInt[originXInit][originYInit + 1][1] == 0)
	{
		return true;
	}	
	else if (mapArrayInt[originXInit + 1][originYInit][1] == 0)
	{
		return true;
	}
	else if (mapArrayInt[originXInit][originYInit - 1][1] == 0)
	{
		return true;
	}
	else if (mapArrayInt[originXInit - 1][originYInit][1] == 0)
	{
		return true;
	}
	else 
	{
		return false;
	}

}

int ACPP_Map::getHarbourRotation(int initXPos, int initYPos)
{
	int originXInit = initXPos - 2;
	int originYInit = initYPos - 2;

	if (mapArrayInt[originXInit][originYInit + 1][1] == 0)
	{
		return 0;
	}
	else if (mapArrayInt[originXInit + 1][originYInit][1] == 0)
	{
		return 270;
	}
	else if (mapArrayInt[originXInit][originYInit - 1][1] == 0)
	{
		return 180;
	}
	else if (mapArrayInt[originXInit - 1][originYInit][1] == 0)
	{
		return 90;
	}
	return 0;
}

bool ACPP_Map::checkBoatInTile(int initXPos, int initYPos)                   //checks if click is next to a boat
{
	if (shipMovementArrayInt[initXPos - 2][(initYPos - 2) + 1][1] == 1)
	{
		embarkXPosInt = initXPos - 2;
		embarkYPosInt = initYPos - 1;
		return true;
	}
	else if (shipMovementArrayInt[(initXPos - 2) + 1][initYPos - 2][1] == 1)
	{
		embarkXPosInt = initXPos - 1;
		embarkYPosInt = initYPos - 2;
		return true;
	}
	else if (shipMovementArrayInt[initXPos - 2][(initYPos - 2) - 1][1] == 1)
	{
		embarkXPosInt = initXPos - 2;
		embarkYPosInt = initYPos - 3;
		return true;
	}
	else if (shipMovementArrayInt[(initXPos - 2) - 1][initYPos - 2][1] == 1)
	{
		embarkXPosInt = initXPos - 3;
		embarkYPosInt = initYPos - 2;
		return true;
	}
	else
	{
		return false;
	}
}

//bool ACPP_Map::checkClickNextToLand(int initXPos, int initYPos)
//{
//	int originXInit = initXPos - 2;
//	int originYInit = initYPos - 2;
//
//	if (mapArrayInt[originXInit][originYInit + 1][1] == 1)
//	{
//		disembarkXPosInt = originXInit;
//		disembarkYPosInt = originYInit + 1;
//		return true;
//	}
//	else if (mapArrayInt[originXInit + 1][originYInit][1] == 1)
//	{
//		disembarkXPosInt = originXInit + 1;
//		disembarkYPosInt = originYInit;
//		return true;
//	}
//	else if (mapArrayInt[originXInit][originYInit - 1][1] == 1)
//	{
//		disembarkXPosInt = initXPos;
//		disembarkYPosInt = initYPos -1;
//		return true;
//	}
//	else if (mapArrayInt[originXInit - 1][originYInit][1] == 1)
//	{
//		disembarkXPosInt = initXPos - 1;
//		disembarkYPosInt = initYPos;
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}

int ACPP_Map::CheckClickNextToLandTile(int initXPos, int initYPos)
{
	int originXInit = initXPos - 2;
	int originYInit = initYPos - 2;

	if (mapArrayInt[originXInit][originYInit + 1][1] == 1)
	{
		return 1;
	}
	else if (mapArrayInt[originXInit + 1][originYInit][1] == 1)
	{
		return 2;
	}
	else if (mapArrayInt[originXInit][originYInit - 1][1] == 1)
	{
		return 3;
	}
	else if (mapArrayInt[originXInit - 1][originYInit][1] == 1)
	{
		return 4;
	}
	else
	{
		return 0;
	}
}

bool ACPP_Map::checkBoatNextToLand(int initXPos, int initYPos)
{
	if (mapArrayInt[initXPos - 2][(initYPos - 2) + 1][1] == 1)
	{
		return true;
	}
	else if (mapArrayInt[(initXPos - 2) + 1][initYPos - 2][1] == 1)
	{
		return true;
	}
	else if (mapArrayInt[initXPos - 2][(initYPos - 2) - 1][1] == 1)
	{

		return true;
	}
	else if (mapArrayInt[(initXPos - 2) - 1][initYPos - 2][1] == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}



////////////////////////////////	MAP SPAWNING FUNCTIONS
int ACPP_Map::GetTileIslandID()		//returns the island ID
{
	return mapArrayInt[gridLocationXInt -2][gridLocationYInt -2][2];
}

int ACPP_Map::GetIslandIDFromLocation(int initXPosInt, int initYPosInt)		//returns the island ID
{
	return mapArrayInt[initXPosInt - 2][initYPosInt - 2][2];
}

int ACPP_Map::GetTileOwner(int initXPosInt, int initYPosInt)		//returns the tile owner
{
	return mapArrayInt[initXPosInt - 2][initYPosInt -2][3];
}

int ACPP_Map::GetTileTerrian()		//returns the terrian type
{
	return mapArrayInt[gridLocationXInt - 2][gridLocationYInt - 2][4];
}

void ACPP_Map::SetTileTerrian(int initXPosInt, int initYPosInt, int initNewTileTerrian)
{
	mapArrayInt[initXPosInt - 2][initYPosInt - 2][4] = initNewTileTerrian;
}

int ACPP_Map::GetTileModel(int initXPosInt, int initYPosInt)		//returns the tile model
{
	return modelArrayInt[initXPosInt - 2][initYPosInt - 2][1];
}

void ACPP_Map::setNewModel(int initXPos, int initYPos, int initModelType)
{
	modelArrayInt[initXPos - 2][initYPos - 2][1] = initModelType;
}

bool ACPP_Map::checkModelInTile(int initXPos, int initYPos)
{
	if (modelArrayInt[initXPos - 2][initYPos - 2][1] == 0)
	{
		return true;
	}
	return false;
}

void ACPP_Map::setIslandOwner(int initIslandIDInt, int initIslandOwner)//sets all the tiles of that island to the same owner
{
	x = 0;
	y = 0;
	while (x < (mapSizeXInt - 1))
	{
		x++;
		y = 0;
		while (y < (mapSizeYInt - 1))
		{
			y++;
			if (mapArrayInt[x][y][2] == initIslandIDInt)		//does that tile have the same island ID that we're looking for
			{
				mapArrayInt[x][y][3] = initIslandOwner;		//set that tile owner to the right one
			}
		}
	}
}

int ACPP_Map::getDisembarkXPos()
{
	return disembarkXPosInt;
}

int ACPP_Map::getDisembarkYPos()
{
	return disembarkYPosInt;
}

void ACPP_Map::clearUnitFromArray(int initXPosInt, int initYPosInt)
{
	unitMovementArrayInt[initXPosInt - 2][initYPosInt - 2][1] = 0;
	unitMovementArrayInt[initXPosInt - 2][initYPosInt - 2][2] = 0;
}



int ACPP_Map::GetTileRescource()
{
	return mapArrayInt[gridLocationXInt - 2][gridLocationYInt - 2][5];
}

void ACPP_Map::SetTileID(int initTileIDInt)
{
	mapArrayInt[gridLocationXInt - 2][gridLocationYInt - 2][6] = initTileIDInt;
}

int ACPP_Map::GetTileID(int initXPosInt, int initYPosInt)
{
	return mapArrayInt[initXPosInt - 2][initYPosInt - 2][6];
}

// Called when the game starts or when spawned
void ACPP_Map::BeginPlay()
{
	Super::BeginPlay();
	//CreateBigIslandSpace();
	
}

int ACPP_Map::GetTileType(int initXPosInt, int initYPosInt)		//retruns the tile type
{
	return mapArrayInt[(initXPosInt - 2)][(initYPosInt - 2)][1];
}

bool ACPP_Map::CreateIslandSpace(int initIslandMaxSizeXInt, int initIslandMinSizeXInt, int initIslandMaxSizeYInt, int initIslandMinSizeYInt,int initIslandCountInt, int initislandMaxInt)
{
	CreateIslandSize(initIslandMaxSizeXInt, initIslandMinSizeXInt, initIslandMaxSizeYInt, initIslandMinSizeYInt);
	AdujstMapSize();


	if (initIslandCountInt < initislandMaxInt)	//checks we don't have too many big islands
	{
		srand(time(NULL));
		randXPosInt = rand() % (mapSizeHighXInt - mapSizeLowXInt + 1) + mapSizeLowXInt;	//randomly gen x Position
		randYPosInt = rand() % (mapSizeHighYInt - mapSizeLowYInt + 1) + mapSizeLowYInt;	//randomly gen y Position

		failedCreateIslandInt = 0;
		while (CheckIslandArea(randXPosInt, randYPosInt, islandSizeXInt, islandSizeYInt) == false)
		{
			if (failedCreateIslandInt < 20)
			{
				randXPosInt = rand() % (mapSizeHighXInt - mapSizeLowXInt + 1) + mapSizeLowXInt;	//randomly gen x Position
				randYPosInt = rand() % (mapSizeHighYInt - mapSizeLowYInt + 1) + mapSizeLowYInt;	//randomly gen y Position
				failedCreateIslandInt++;
			}
			else
			{
				return false;
			}
		}

		randStartXPosInt = randXPosInt;		///sets are x and y starting position
		randStartYPosInt = randYPosInt;

		while (randXPosInt < (randStartXPosInt + islandSizeXInt))		//araea where we set our area
		{
			randYPosInt = randStartYPosInt;
			while (randYPosInt < (randStartYPosInt + islandSizeYInt))
			{
				firstArrayInt[randXPosInt][randYPosInt][1] = 1;		//fills the area in the firstarray
				randYPosInt++;
			}
			randXPosInt++;
		}
		SpawnIsland(randStartXPosInt, randStartYPosInt, islandSizeXInt, islandSizeYInt, (rand() % (spawnIslandMaxTilesInt - spawnIslandMinTilesInt + 1) + spawnIslandMinTilesInt));
		return true;
	}
	return false;
}

void ACPP_Map::CreateIslandSize(int initIslandMaxSizeXInt, int initIslandMinSizeXInt, int initIslandMaxSizeYInt, int initIslandMinSizeYInt)
{
	srand(time(NULL));
	oldIslandSizeXInt = islandSizeXInt;
	oldIslandSizeYInt = islandSizeYInt;
	islandSizeXInt = rand() % (initIslandMaxSizeXInt - initIslandMinSizeXInt + 1) + initIslandMinSizeXInt;	//creates random size	
	islandSizeYInt = rand() % (initIslandMaxSizeYInt - initIslandMinSizeYInt + 1) + initIslandMinSizeYInt;
	while (oldIslandSizeXInt == islandSizeXInt && oldIslandSizeYInt == islandSizeYInt)	// checks its not the same size as the last one
	{
		islandSizeXInt = rand() % (initIslandMaxSizeXInt - initIslandMinSizeXInt + 1) + initIslandMinSizeXInt;	//creates random size
		islandSizeYInt = rand() % (initIslandMaxSizeYInt - initIslandMinSizeYInt + 1) + initIslandMinSizeYInt;
	}
	spawnIslandMaxTilesInt = ((islandSizeXInt - 2) * (islandSizeYInt - 2)) - (((islandSizeXInt - 2) + (islandSizeYInt - 2)) / 2);	//sets the max amount of tiles	
	spawnIslandMinTilesInt = spawnIslandMaxTilesInt - 5; //sets the min amount of tiles
	//if (spawnIslandMinTilesInt <= )	//checks theirs more than 10 tiles
	//{
	//	spawnIslandMinTilesInt = 10;
	//}
}

bool ACPP_Map::CheckIslandArea(int initXPosInt, int initYPosInt, int initIsalndSizeXInt, int initIslandSizeYInt)
{
	checkStartXPosInt = initXPosInt;		//sets the starting area x and y
	checkStartYPosInt = initYPosInt;
	while (initXPosInt < checkStartXPosInt + initIsalndSizeXInt)
	{
		initYPosInt = checkStartYPosInt;
		while (initYPosInt < checkStartYPosInt + initIslandSizeYInt)		//loops through the area checking its all free
		{
			if (firstArrayInt[initXPosInt][initYPosInt][1] == 1)
			{
				return false;		///returns false if the area isn't free
			}
			initYPosInt++;
		}
		initXPosInt++;
	}
	return true;
}

void ACPP_Map::SpawnIsland(int initStartXPosInt, int initStartYPosInt, int initIslandSizeXInt, int initIslandSizeYInt, int initTileAmountInt)
{
	rainForestChanceInt = rand() % (8 - 5 + 1);
	spawnTilesCountInt = 0;

	spawnHighXPosInt = ((initStartXPosInt + initIslandSizeXInt) - 1);			// works out the high and low positions within the areas 
	spawnLowXPosInt = initStartXPosInt + 2;
	spawnHighYPosInt = (initStartYPosInt + initIslandSizeYInt) - 1;
	spawnLowYPosInt = initStartYPosInt + 2;

	while (spawnTilesCountInt < initTileAmountInt)
	{
		spawnRandXPosInt = rand() % (spawnHighXPosInt - spawnLowXPosInt + 1) + spawnLowXPosInt;			///randomly places land tiles
		spawnRandYPosInt = rand() % (spawnHighYPosInt - spawnLowYPosInt + 1) + spawnLowYPosInt;

		while (mapArrayInt[spawnRandXPosInt][spawnRandYPosInt][1] == 1)
		{
			spawnRandXPosInt = rand() % (spawnHighXPosInt - spawnLowXPosInt + 1) + spawnLowXPosInt;		///checks their isn't a land tile their already
			spawnRandYPosInt = rand() % (spawnHighYPosInt - spawnLowYPosInt + 1) + spawnLowYPosInt;
		}
		mapArrayInt[spawnRandXPosInt][spawnRandYPosInt][1] = 1;
		mapArrayInt[spawnRandXPosInt][spawnRandYPosInt][2] = islandIDInt;
		mapArrayInt[spawnRandXPosInt][spawnRandYPosInt][3] = 0;
		mapArrayInt[spawnRandXPosInt][spawnRandYPosInt][4] = SpawnForest(rainForestChanceInt);

		spawnTilesCountInt++;
	}
	SpawnCity();
	islandIDInt++;
}

void ACPP_Map::SpawnCity()
{
	while (playerCountInt < playerMaxAmountInt)
	{
		spawnRandXPosInt = rand() % (spawnHighXPosInt - spawnLowXPosInt + 1) + spawnLowXPosInt;			///randomly pciks a tile to spawn the city on
		spawnRandYPosInt = rand() % (spawnHighYPosInt - spawnLowYPosInt + 1) + spawnLowYPosInt;
		while (mapArrayInt[spawnRandXPosInt][spawnRandYPosInt][1] == 0)
		{
			spawnRandXPosInt = rand() % (spawnHighXPosInt - spawnLowXPosInt + 1) + spawnLowXPosInt;		///checks this tile is a land tile
			spawnRandYPosInt = rand() % (spawnHighYPosInt - spawnLowYPosInt + 1) + spawnLowYPosInt;
		}
		firstCityX = spawnRandXPosInt;
		firstCityY = spawnRandYPosInt;
		modelArrayInt[spawnRandXPosInt][spawnRandYPosInt][1] = 1;		//sets this place in model array to have a city on it
		for (int x = 1; x < mapSizeXInt - 1;x++)		
		{
			for (int y = 1; y < mapSizeYInt - 1;y++)		
			{													//changes all the tiles to have this player owner!!
				if (mapArrayInt[x][y][2] == islandIDInt)
				{
					mapArrayInt[x][y][3] = (playerCountInt + 1);
				}
			}
		}
		playerCountInt++;
	}
}

void ACPP_Map::AdujstMapSize()		//changes the spawable map size, stopping islands spawning to close to the edge of the map
{
	mapSizeHighXInt = (mapSizeXInt - islandSizeXInt) - 3;
	mapSizeLowXInt = 3;
	mapSizeHighYInt = (mapSizeYInt - islandSizeYInt) - 3;
	mapSizeLowYInt = 3;
}

int ACPP_Map::SpawnForest(int initRainForestChance)		//spawns rainforest onto the map. depending on the chance input value
{
	rainForestChanceInt = rand() % (10 - 1 + 1);
	if (rainForestChanceInt < 6)
	{
		return 2;
	}
	else
	{
		return 1;
	}
}

bool ACPP_Map::CheckIslandForResources(int initIslandID, int initRescourceID)		//checks if island already has resources on
{
	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (mapArrayInt[x][y][2] == initIslandID && mapArrayInt[x][y][5] == initRescourceID)	//checking every tile which matches the island ID has the same rescource ID on it 
			{
				return false;
			}
		}
	}
	return true;
}

void ACPP_Map::SpawnResources(int initSpawnAmountInt, int initSpawnMaxAmountInt, int initIslandSpawnCountInt, int initIslandMaxSpawnableInt, int initSpawnIslandIDInt
	, int initIslandFailedCountInt, bool initSpawnedOnIslandBool,int initResourceIDInt, int initSpawnChanceInt, int initSpawnMaxChanceInt)		//spawns rescources
{
	while (initIslandSpawnCountInt < initIslandMaxSpawnableInt)		//while the island spawnable count is less than maxium a amount of island we can spawn rescources is true 
	{
		initSpawnIslandIDInt = rand() % (islandAmountInt - 1 + 1);		//pick a random island
		initSpawnIslandIDInt--;		
		initSpawnAmountInt = 0;
		initSpawnedOnIslandBool = false;
		initIslandFailedCountInt = 0;
		while (CheckIslandForResources(initSpawnIslandIDInt, initResourceIDInt) == false && initIslandFailedCountInt < (initIslandMaxSpawnableInt * 4))		//checks island doesn't already have the same rescouce on
		{
			initSpawnIslandIDInt = rand() % (islandAmountInt - 1 + 1);		//picks a new island
			initSpawnIslandIDInt--;
			initIslandFailedCountInt++;
		}
		if (initIslandFailedCountInt < (islandAmountInt * 2))		// fail statement so we don't get stuck in a loop
		{
			for (int x = 0; x < (mapSizeXInt - 1); x++)
			{
				for (int y = 0; y < (mapSizeYInt - 1); y++)
				{
					if (mapArrayInt[x][y][2] == initSpawnIslandIDInt && mapArrayInt[x][y][1] == 1 && initSpawnAmountInt < initSpawnMaxAmountInt)	//checks tile is apart of island, check its a land tile and we haven't spawn all the rescources we can
					{
						initSpawnChanceInt = rand() % (20 - 1 + 1);		//random roll to see if we should spawn a rescource
						if (initSpawnChanceInt < initSpawnMaxChanceInt) 
						{
							initSpawnAmountInt++;
							mapArrayInt[x][y][5] = initResourceIDInt;		//sets that tile to have that rescouce on it
							initSpawnedOnIslandBool = true;			
						}
					}
				}
			}
			if (initSpawnedOnIslandBool == true)		//if we have spawn a resource on this island then spawn island count ++
			{
				initIslandSpawnCountInt++;
			}
		}
	}
}

int ACPP_Map::getCityTileIDOnIsland(int tileOwnerID, int initIslandID)
{
	int cityTileID = 0;

	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (mapArrayInt[x][y][2] == initIslandID && mapArrayInt[x][y][3] == tileOwnerID && modelArrayInt[x][y][1] == 1)
			{
				cityTileID = mapArrayInt[x][y][6];
			}
		}
	}

	return cityTileID;
}

int ACPP_Map::getFirstCityActualX()
{
	int actualX;
	actualX = firstCityX * 200;

	return actualX;
}

int ACPP_Map::getFirstCityActualY()
{
	int actualY;
	actualY = firstCityY * 200;

	return actualY;
}

int ACPP_Map::getCityCount()
{
	cityCount = 0;

	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (modelArrayInt[x][y][1] == 1)
			{
				cityCount++;
			}
		}
	}

	return cityCount;
}

int ACPP_Map::getNumberOfLandUnits()
{
	int numberOfLandUnits = 0;

	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (unitMovementArrayInt[x][y][1] > 0)
			{
				numberOfLandUnits++;
			}
		}
	}

	return numberOfLandUnits;
}

int ACPP_Map::getNumberOfLandUnitsWithMovementLeft()
{
	int numberOfLandUnits = 0;

	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (unitMovementArrayInt[x][y][1] > 0 && unitMovementArrayInt[x][y][3] > 0)
			{
				numberOfLandUnits++;
			}
		}
	}

	return numberOfLandUnits;
}

int ACPP_Map::getNumberOfSeaUnits()
{
	int numberOfSeaUnits = 0;

	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (shipMovementArrayInt[x][y][1] > 0)
			{
				numberOfSeaUnits++;
			}
		}
	}

	return numberOfSeaUnits;
}

int ACPP_Map::getNumberOfSeaUnitsWithMovementLeft()
{
	int numberOfSeaUnits = 0;

	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (shipMovementArrayInt[x][y][1] > 0 && shipMovementArrayInt[x][y][3] > 0)
			{
				numberOfSeaUnits++;
			}
		}
	}

	return numberOfSeaUnits;
}

void ACPP_Map::setLandUnitsMovement(int unitType, int unitID)
{
	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (unitMovementArrayInt[x][y][1] == unitType && unitMovementArrayInt[x][y][2] == unitID)
			{
				unitMovementArrayInt[x][y][3] = 1;
			}
		}
	}
}

void ACPP_Map::setSeaUnitsMovement(int seaUnitType, int seaUnitID)
{
	for (int x = 0; x < (mapSizeXInt - 1); x++)
	{
		for (int y = 0; y < (mapSizeYInt - 1); y++)
		{
			if (shipMovementArrayInt[x][y][1] == seaUnitType && shipMovementArrayInt[x][y][2] == seaUnitID)
			{
				shipMovementArrayInt[x][y][3] = 1;
			}
		}
	}
}

// Called every frame
//void ACPP_Map::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//
//}
//
