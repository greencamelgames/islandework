// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_MineMetal.h"


// Sets default values
ACPP_MineMetal::ACPP_MineMetal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	metalGenPerTurnInt = 1;
}

// Called when the game starts or when spawned
void ACPP_MineMetal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_MineMetal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

