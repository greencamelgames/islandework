// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_GameLoop.h"


// Sets default values
ACPP_GameLoop::ACPP_GameLoop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	currentTurn = 1;
	currentGold = 0;
	goldFlow = 0;

}

// Called when the game starts or when spawned
void ACPP_GameLoop::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_GameLoop::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_GameLoop::NextTurn()
{
	currentTurn = currentTurn + 1;
	currentGold = currentGold + goldFlow;
}