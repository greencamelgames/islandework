// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Harbour.generated.h"

UCLASS()
class ISLANDEXPEDITION_API ACPP_Harbour : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_Harbour();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HarbourInfo")
		int cost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HarbourInfo")
		int boatTally;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HarbourInfo")
		int boatType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HarbourInfo")
		int boats;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HarbourInfo")
		int unitsH;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HarbourInfo")
		int transferrables;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HarbourInfo")
		int fleetHealth;
	

//public:	
//	// Called every frame
//	virtual void Tick(float DeltaTime) override;

	
	
};
