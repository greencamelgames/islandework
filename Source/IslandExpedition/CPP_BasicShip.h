// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_BasicShip.generated.h"

UCLASS()
class ISLANDEXPEDITION_API ACPP_BasicShip : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_BasicShip();

	int getUnitMaxMovesBoat(void);
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShipMovement")
		bool firstClick;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShipMovement")
		int shipMaxMovesInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShipMovement")
		int shipMovesLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShipMovement")
		int moveCost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShipMovement")
		int shipIDInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShipMovement")
		int shipTypeInt;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CrewNeeds")
		int maxCrewRequired;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CrewNeeds")
		bool captainNeeded;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CrewNeeds")
		int foodAvailableOnBoard;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Storage")
		int storageSpaceAvailable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ResourceRequirements")
		int goldPerTurn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ResourceRequirements")
		int foodRequiredOnBoard;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ResourceRequirements")
		bool requirementsMet;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonuses")
		int movementBonus;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitShipStats")
		bool unitOnBoard;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitShipStats")
		int unitMaxMovesInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitShipStats")
		int unitMovesLeftInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitShipStats")
		int unitTypeInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitShipStats")
		int moveCostBoat;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitShipStats")
		int unitID;

	UFUNCTION(BlueprintCallable)
		void ResetTurn(int initShipMaxMoves);
	UFUNCTION(BlueprintCallable)
		void ClickDistance(int oldXPosInt, int oldYPosInt, int newXPos, int newYPos);
	UFUNCTION(BlueprintCallable)
		bool WithinRange(int initMoveCost, int initMovesLeft);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	int moveXDifInt;
	int moveYDifInt;

	
	
};
