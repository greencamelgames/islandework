// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_BasicLandUnit.generated.h"

UCLASS()
class ISLANDEXPEDITION_API ACPP_BasicLandUnit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_BasicLandUnit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		bool firstClick;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int movementAllowed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int unitMaxMovesInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int unitMovesLeftInt;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int unitTypeInt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int moveCost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UnitMovement")
		int unitID;
	UFUNCTION(BlueprintCallable)
		void ResetTurn(int initUnitMaxMoves);
	UFUNCTION(BlueprintCallable)
		void ClickDistance(int oldXPosInt, int oldYPosInt, int newXPos, int newYPos);
	UFUNCTION(BlueprintCallable)
		bool WithinRange(int initMoveCost, int initMovesLeft);

	UFUNCTION(BlueprintCallable)
		int getMovement();
	UFUNCTION(BlueprintCallable)
		void setMovement(int allowed);

	UFUNCTION(BlueprintCallable)
		void checkMovement();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	int moveXDifInt;
	int moveYDifInt;

	
	
};
