// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_BasicFoodProduction.h"


// Sets default values
ACPP_BasicFoodProduction::ACPP_BasicFoodProduction()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	foodGenPerTurnInt = 1;

}

// Called when the game starts or when spawned
void ACPP_BasicFoodProduction::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_BasicFoodProduction::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

